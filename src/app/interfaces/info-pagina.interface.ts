export interface InfoPagina {
  dni?: string;
  nombre?: string;
  apellido?: string;
  edad?: string;
}