import { InfoPagina } from './../interfaces/info-pagina.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {

  info: InfoPagina = {};
  // cargada = false; 
  equipo: any[] = [];

  constructor( private http: HttpClient) { 
    // this.cargarInfo();
    this.cargarEquipo();
  }

  // private cargarInfo(){
  //   this.http.get('assets/data/data-pagina.json')
  //     .subscribe((result: InfoPagina) => {
  //       this.cargada = true;
  //       this.info = result;
  //     });
  // }

  private cargarEquipo(){
    // this.http.get('https://api-apross.herokuapp.com/api/personas.json') //from Heroku
      this.http.get('http://localhost:3000/api/personas.json') // localhost:3000
      .subscribe((result: any[]) => {
        this.equipo = result;
        console.log(result);

      });
  }
}
